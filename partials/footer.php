	<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<div class="content">
						<section>
							<h3>About Us</h3>
							<p>Uchews was started in 2018 with one simple goal in mind, to make finding food easy, enjoyable, and safe for anyone with dietary restrictions. With 1 in 13 children suffering from a food allergy and millions of others being affected by some sort of medical dietary restriction, the need for Uchews' simple and intuitive system is very clear.</p>
						</section>
						<section>
							<h4>Contact</h4>
							<ul class="alt">
								<li><a href="#"><br />Email: customerservice@uchews.com</a></li>
								<li><a href="#">Phone: 865-234-2341</a></li>
								<li><a href="#">Address: 4312 Short Lane, Knoxville, TN</a></li>
							</ul>
						</section>
						<section>
							<h4>Follow Uchews</h4>
							<ul class="plain">
								<li><br /><a href="#"><i class="icon fa-twitter">&nbsp;</i>Twitter</a></li>
								<li><a href="#"><i class="icon fa-facebook">&nbsp;</i>Facebook</a></li>
								<li><a href="#"><i class="icon fa-instagram">&nbsp;</i>Instagram</a></li>
								<li><a href="#"><i class="icon fa-github">&nbsp;</i>Github</a></li>
							</ul>
						</section>
					</div>
					<div class="copyright">
						Copyright &copy; 2018 Uchews. All rights reserved. 
					</div>
				</div>
			</footer>