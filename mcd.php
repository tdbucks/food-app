<!DOCTYPE HTML>
<!--
	Industrious by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<body class="is-preload">
	<head>
		<title>Uchews: Personal Food Finder</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<?php

		include_once('partials/header.php');

	?>
	
	<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>McDonald's</h2>
						<p>1720 W Cumberland Ave, Knoxville, TN 37916</p>
					</header>
	    <ul>
	        <li>McDonald's Fruit & Maple Oatmeal without Brown Sugar - 49g Carbs</li>
	        <li>McDonald's Premium Asian Salad w/ 18g or w/o Grilled Chicken - 13g Carbs</li>
			<li>McDonald's Premium Bacon Ranch Salad w/ 9g or w/o Grilled Chicken - 9g Carbs</li>
			<li>McDonald's Cuties - 8g Carbs</li>
			<li>McDonald's Apple Dippers - 4g Carbs</li>
			<li>McDonald's Side Salad - 4g Carbs</li>
			<li>McDonald's Strawberry Gogurt - 9g Carbs</li>

	    </ul>
	    
	    <a href="https://www.mcdonalds.com/us/en-us/full-menu.html?cid=PS:GCM_Ret:B::Google:mcdonalds_menu" target="_blank">View Full Menu</a>
	    </section>
	    
	    
	
	
	
	
	<?php

		include_once('partials/footer.php');

	?>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>