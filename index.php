<!DOCTYPE HTML>
<!--
	Industrious by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<body class="is-preload">
	<head>
		<title>Uchews: Personal Food Finder</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<?php

		include_once('partials/header.php');

	?>
		<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<h1>Uchews</h1>
					<p>A universal menu designed for you and your specific dietary intolerances, limits, and conditions.<br />
					Designed by the AyyMD team for VolHacks.<br />
					<br />
						<a href="startup.php" class="button primary">Get started</a>
					</p>
				</div>
			</section>

		<!-- Highlights -->
			<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>Supported Dietary Conditions</h2>
						<p>These are the current dietary conditions that have been implemented into Uchews. This list will constantly be expanding as you bring us your specific dietary needs!</p>
					</header>
					<div class="highlights">
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-heart-o"><span class="label">Icon</span></a>
									<h3>Celiac Disease</h3>
								</header>
								<p>Gluten-Free Diet. Select Gluten from the zero tolerance ingredients during profile creation.</p>
							</div>
						</section>
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-heart-o"><span class="label">Icon</span></a>
									<h3>Diabetes</h3>
								</header>
								<p>Low-Carb Diet based around strict mealtime carb amounts. Set your carb limit for meals during profile creation. </p>
							</div>
						</section>
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-heart-o"><span class="label">Icon</span></a>
									<h3>Lactose Intolerance</h3>
								</header>
								<p>Lactose-free Diet. Select Dairy from the zero tolerance ingredients during profile creation.</p>
							</div>
						</section>
				
					</div>
				</div>
			</section>

	<?php

		include_once('partials/footer.php');

	?>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>