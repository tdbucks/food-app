<!DOCTYPE HTML>
<!--
	Industrious by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<body class="is-preload">
	<head>
		<title>Uchews: Personal Food Finder</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<?php

		include_once('partials/header.php');

	?>

		<!-- Medical Options -->
		<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>Results</h2>
						<p>Here are a list of restaurants and markets near you that fit your needs</p>
					</header>
                    <ul>
					<li><h2><a href="mcd.php">McDonalds</a></h2></li>
				    <br />
				    <li><h2>Chick-fil-a</h2></li>
				    </ul>
					</div>
				</div>
			</section>

		<?php

		include_once('partials/footer.php');

		?>
		
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>