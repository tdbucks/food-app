This app's goal is to find restaurants for people with dietary needs and restrictions.
This app is a project developed for the 2018 UTK Hackathon.

Please use PHP with Apache for best results.
Thanks for trying our app!