<!DOCTYPE HTML>
<!--
	Industrious by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<body class="is-preload">
	<head>
		<title>Uchews: Personal Food Finder</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<?php

		include_once('partials/header.php');

	?>
		<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<h1>Profile Creation</h1>
					<p>Your profile. Your diet. Your life.<br />
					<br />
					</p>
				</div>
			</section>

		<!-- Medical Options -->
		<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>Build your profile</h2>
						<p>Please select your medical and dietary preferences</p>
					</header>

					
									<form method="post" action="#">
										<div class="row gtr-uniform">
											<div class="col-6 col-12-xsmall">
												<input type="text" name="name" id="name" value="" placeholder="Name" />
											</div>
											<div class="col-6 col-12-xsmall">
												<input type="email" name="email" id="email" value="" placeholder="Email" />
											</div>
											<!-- Break -->
											
											<div class="row">
												<div class="col-6 col-12-medium">
								
												<!-- Break -->
												<div class="inner" align="left">
													<h3>Zero Tolerance Foods</h3>
													<br />
												
												<div class="col-6 col-12-small">
													<input type="checkbox" id="checkbox-alpha" name="checkbox">
													<label for="checkbox-alpha">Gluten</label>
												</div>
												<div class="col-6 col-12-small">
													<input type="checkbox" id="checkbox-beta" name="checkbox">
													<label for="checkbox-beta">Dairy</label>
												</div>
												<!-- Break -->
												<div class="col-12">
													<h3>Other</h3>
													<textarea name="textarea" id="textarea" placeholder="Add your other intolerances here to help us expand our data to better serve you!" rows="6"></textarea>
												</div>
												</div>
												</div>
												<!--place extra stuff after this comment to go in right side column -->
												<div class="col-6 col-12-medium">
													<h3>Carb Limit for Meals</h3>
												<select name="category" id="category">
													<option value="">- Select -</option>
													<option value="">none</option>
													<option value="alpha">10</option>
													<option value="beta">20</option>
													<option value="gamma">30</option>
													<option value="delta">40</option>
													<option value="epsilon">50</option>
													<option value="zeta">60</option>
													<option value="eta">70</option>
												</select>
												</div>
												
												</div>
												<!-- Break -->
											<div class="col-12">
												<ul class="actions">
													<li><a href="results.php" class="button primary">Find my nearby food!</a></li>
													<li><input type="reset" value="Reset" /></li>
												</ul>
											</div>
										</div>
									</form>	
				
					</div>
				</div>
			</section>

		<?php

		include_once('partials/footer.php');

		?>
		
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>